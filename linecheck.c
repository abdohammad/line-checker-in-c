/*
  The linecheck program is used to analyze each line of an inputted
  file and output the length of each individual line. The length will
  include standard characters and tab characters. 
  Author: Abdelrahman Hammad       Student ID: 112394561
  Section: 0104                    TA: Mathew
 */
#include <stdio.h>
#define MAX 1000

/*
 * Main Function
 */

int main () {
  /*
    Variable Declarations:
    line_length: Determines how many characters a line has and takes 
    into account tab characters
    read_counter: A counter variable used to keep track of how 
    many values were read
    print_counter: A counter variable used to keep track of how
    many values were printed
    line_holder: A character array that holds the characters 
    of an array
    char_holder: A character variable that holds each current 
    scanned value
   */
  int line_length = 0;
  int print_counter = 0;
  int read_counter = 0;
  char line_holder [MAX];
  char char_holder; 

  /*
    The loop will continue to run untill we reach the end of the file.
   */
  while(!feof(stdin)) {
    scanf("%c", &char_holder);
    line_holder[read_counter] = char_holder;
    /*
      Only when the character is not a new line character do we enter 
      the loop and process modify the line_length and read_counter variables
     */
    while(char_holder != '\n') {
      /*
	If the character is a tab then change length to closest multiple of 8
	else increase the length by 1
       */
      if(char_holder == '\t')
	line_length = (((line_length/8) + 1) * 8);
      else 
	line_length++;
      read_counter++;
      scanf("%c", &char_holder);
      line_holder[read_counter] = char_holder;
    }
    /*
      This if statement is used to ignore the last space right before
      the end of file marker, which is technically not considered a 
      line
     */
    if(!feof(stdin)) {
      /*
	Prints the size of the line, then all the contents of that line
       */
      printf("%4d: ", line_length);
      for(print_counter = 0; print_counter < read_counter + 1; print_counter ++)
	printf("%c", line_holder[print_counter]);
    }
    /*
      The read_counter, print_counter, line_length counter are all reset 
      to zero to handle the next incoming line in the file
     */
    read_counter = 0;
    print_counter = 0;
    line_length = 0;
  }

  
  return 0;
  
}
